# microG Mobile Services

This is a collection of FOSS APKs, coupled with the respective Makefiles for an
easy integration in the Android build system.

To include them in your build:
1. add a repo manifest file to include this repository and the [android_prebuilts_prebuiltapks](https://codeberg.org/iNeedADoctor/android_prebuilts_prebuiltapks) repository, as `vendor/partner_gms`. (see below)
2. set `WITH_GMS` to `true` when building.


## Manifests

Manifest for LineageOS **14.1** and **15.1**:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote name="codeberg" fetch="https://codeberg.org"/>

    <project name="iNeedADoctor/android_vendor_partner_gms" path="vendor/partner_gms" remote="codeberg" revision="master" />
    <project name="iNeedADoctor/android_prebuilts_prebuiltapks" path="prebuilts/prebuiltapks" remote="codeberg" revision="lineage-14.1" />
</manifest>
```

Manifest for LineageOS => **16.0** and <= **18.1**:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote name="codeberg" fetch="https://codeberg.org"/>

    <project name="iNeedADoctor/android_vendor_partner_gms" path="vendor/partner_gms" remote="codeberg" revision="master" />
    <project name="iNeedADoctor/android_prebuilts_prebuiltapks" path="prebuilts/prebuiltapks" remote="codeberg" revision="lineage-16.0" />
</manifest>
```

Manifest for LineageOS **19.0** and **19.1**:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote name="codeberg" fetch="https://codeberg.org"/>

    <project name="iNeedADoctor/android_vendor_partner_gms" path="vendor/partner_gms" remote="codeberg" revision="master" />
    <project name="iNeedADoctor/android_prebuilts_prebuiltapks" path="prebuilts/prebuiltapks" remote="codeberg" revision="lineage-19.0" />
</manifest>
```

Manifest for LineageOS **20.0**:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote name="codeberg" fetch="https://codeberg.org"/>

    <project name="iNeedADoctor/android_vendor_partner_gms" path="vendor/partner_gms" remote="codeberg" revision="master" />
    <project name="iNeedADoctor/android_prebuilts_prebuiltapks" path="prebuilts/prebuiltapks" remote="codeberg" revision="lineage-20.0" />
</manifest>
```


## Applications

Note: You do not need to set `CUSTOM_PACKAGES` for the packages to be included when building with [iNeedADoctor/docker-lineage](https://codeberg.org/iNeedADoctor/docker-lineage).

The included APKs are:
 * FDroid packages (binaries sourced from [here](https://f-droid.org/packages/org.fdroid.fdroid/) and [here](https://f-droid.org/packages/org.fdroid.fdroid.privileged/))
   * FDroid: a catalogue of FOSS (Free and Open Source Software) applications for the Android platform
   * FDroid Privileged Extension: a FDroid extension to ease the installation/removal of apps
   * additional_repos.xml: a simple package to include the [microG FDroid repository](https://microg.org/fdroid.html) in the ROM (requires FDroid >= 1.5)
 * microG packages (binaries sourced from [here](https://microg.org/download.html))
   * MicroGServices: the main component of microG, a FOSS reimplementation of the Google Play Services (requires GsfProxy and FakeStore for full functionality)
   * GsfProxy: a GmsCore proxy for legacy GCM compatibility
   * MicroGCompanion: an empty package that mocks the existence of the Google Play Store
 * AuroraOSS packages (binaries sourced from [here](https://gitlab.com/AuroraOSS))
   * AuroraStore: alternative for Google Play Store

These are official unmodified prebuilt binaries, signed by the
corresponding developers.
